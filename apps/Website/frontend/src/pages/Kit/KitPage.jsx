import * as React from 'react';
import { useEffect, useState } from "react";
import { Title } from "../../components/atoms/TitleComponent/Title";
import { ApiHttpRequest } from "../../services/ApiHttpClient";

import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Paragraph } from '../../components/atoms/ParagraphComponent/Paragraph';

// TODO: edit Kit -> PUT /kits/{kitId} (send name and location_name)

export function KitPage({ params }) {
    const [wateringHistory, setWateringHistory] = useState([]);
    const [userKit, setUserKit] = useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    useEffect(() => {
        const fetchWateringHistory = async () => {
            const responseJson = await ApiHttpRequest(`/watering_history/${params.id}`);
            setWateringHistory(responseJson)
        }

        const fetchUserKit = async () => {
            const userKit = await ApiHttpRequest(`/kits/${params.id}`);
            setUserKit(userKit);
        }

        fetchWateringHistory()
        fetchUserKit()
    }, [])

    const columns = [
        { id: 'date_start', label: 'Date Start', minWidth: 100 },
        { id: 'date_end', label: 'Date End', minWidth: 100 },
        { id: 'soil_moisture', label: 'Soil Moisture', minWidth: 100 },
        { id: 'temperature', label: 'Temperature', minWidth: 100 }
    ];

    const rows = [];
    let status = wateringHistory?.status !== 'KO';

    if (status) {
        wateringHistory.forEach((wateringHistoryRegistry) => {
            rows.push({
                date_start: wateringHistoryRegistry.date_start ?? '-',
                date_end: wateringHistoryRegistry.date_end ?? '-',
                soil_moisture: wateringHistoryRegistry.soil_moisture ?? '-',
                temperature: wateringHistoryRegistry.temperature ?? '-'
            })
        })
    }

    return(
        <>
            <Title
                title={`Kit ${userKit.name ?? 'X'} watering history`}
            />
            {
                status
                    ? <Paper sx={{ width: '90%', overflow: 'hidden', margin: '0 auto' }}>
                        <TableContainer sx={{ maxHeight: 440 }}>
                            <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                    >
                                    {column.label}
                                    </TableCell>
                                ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row) => {
                                    return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                        {columns.map((column) => {
                                        const value = row[column.id];
                                        return (
                                            <TableCell key={column.id} align={column.align}>
                                            {column.format && typeof value === 'number'
                                                ? column.format(value)
                                                : value}
                                            </TableCell>
                                        );
                                        })}
                                    </TableRow>
                                    );
                                })}
                            </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[10, 25, 100]}
                            component="div"
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                    : <Paragraph
                                text="No Watering History could be retrieved"
                    />
            }
        </>
    )
}