import { useUser } from "../../context/UserContext"

export function LogoutPage() {
    const userContext = useUser();

    userContext.setUser(false);
    return(
        <div>Bye user</div>
    )
}