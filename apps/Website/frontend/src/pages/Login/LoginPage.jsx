import Login from "../../components/organisms/LoginComponent/Login"
import { useUser } from "../../context/UserContext"
import SuperTokens from "supertokens-auth-react";


export function LoginPage() {
    const userContext = useUser();

    userContext.setUser(true);
    
    if (SuperTokens.canHandleRoute()) 
        return SuperTokens.getRoutingComponent()
    
    return(
      <Login />      
    )
}