import { Title } from "../../components/atoms/TitleComponent/Title";

export function HomePage() {
    return(
        <section style={
            {
                "display": "flex",
                "flexDirection": "column",
                "alignItems": "center",
                "justifyContent": "center",
                "height": "90%"
            }
        }>
            <Title
                title={"Welcome!"}
            />
        </section>
    )
}