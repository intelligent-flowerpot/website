import { useEffect, useState } from "react";
import { Title } from "../../components/atoms/TitleComponent/Title";
import { ListOfElementsWithLink } from "../../components/organisms/ListOfElementsWithLinkComponent/ListOfElementsWithLink";
import { ApiHttpRequest } from "../../services/ApiHttpClient";

export function ListOfKitsPage() {
    const [userKits, setUserKits] = useState([]);
    const [kitsSensorData, setKitsSensorData] = useState([]);

    // TODO: get this info from api call -> GET /users/{userId}/kits
    useEffect(() => {
        const fetchUserKits = async () => {
            const userKits = await ApiHttpRequest('/users/1/kits');
            setUserKits(userKits);
            return userKits;
        }

        // TODO: get sensor data from kit -> GET /sensor_data_history/{kitId}/last
        const fetchKitsSensorData = async () => {
            const userKits = await fetchUserKits();
            let kitsSensorData = [];

            for (const userKit of userKits) {
                const kitSensorData = await ApiHttpRequest(`/sensor_data_history/${userKit.kit_id}/last`)
                kitsSensorData.push(kitSensorData);
            }

            setKitsSensorData(kitsSensorData);
        }
        fetchKitsSensorData();
    }, [])

    const kits = [];

     // TODO: soil_mosture value * 100% / max soil_mosture
    userKits.forEach((kit, index) => {
        kits.push({
                id: kit?.kit_id ?? '',
                name: kit?.name ?? '',
                location: kit?.location_name ?? '',
                value: kitsSensorData[index]?.soil_moisture ? Math.floor(kitsSensorData[index]?.soil_moisture * -100 / 4095) : -1
        });
    })

    return (
        <>
            <section>
                <Title
                    title="Your Kits"
                />
                <ListOfElementsWithLink
                    elements={kits}
                />
            </section>
        </>
    )
}