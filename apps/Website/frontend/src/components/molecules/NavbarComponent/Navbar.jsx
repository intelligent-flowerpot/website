import './Navbar.scss'
import NavbarItem from '../../atoms/NavbarItemComponent/NavbarItem';
import { useUser } from '../../../context/UserContext';

export default function Navbar() {
    const userContext = useUser();

    return (
        <nav className='navbar'>
          <NavbarItem
            path="/"
            text="Home"
          />
          <NavbarItem
            path={userContext.user ? "/kits" : "/signup"}
            text={userContext.user ? "Kits" : "SignUp"}
            />
          <NavbarItem
            path={userContext.user ? "/logout" : "/login"}
            text={userContext.user ? "Logout" : "Login"}
          />
        </nav>
    );
}