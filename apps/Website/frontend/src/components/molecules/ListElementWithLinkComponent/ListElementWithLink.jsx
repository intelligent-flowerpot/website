import './ListElementWithLink.scss';
import { Link } from 'wouter';
import { Wave } from '../../atoms/WaveComponent/Wave';
import { Paragraph } from '../../atoms/ParagraphComponent/Paragraph';

export function ListElementWithLink({ id, name, value }) {
    return(
        <li className="listElementWithLink">
            <p>{name}</p>
            <Link to={`/kit/${id}`} className="listElementWithLink__link">
                <Paragraph
                    text={`${Math.abs(value)}%`}
                    className="listElementWithLink__value"
                />
                <Wave 
                    top={value}
                />
            </Link>
        </li>
    )
}