import { Link } from 'wouter'
import './NavbarItem.scss'

export default function NavbarItem({path, text}) {
    return(
        <li className="navbarItem">
            <Link
                to={path}
                className="navbarItem__link navbarItem__link--white navbarItem__link--uppercase"
            >
                {text}
            </Link>
        </li>
    )
}
