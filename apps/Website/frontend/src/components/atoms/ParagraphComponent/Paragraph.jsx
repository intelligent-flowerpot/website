import './Paragraph.scss';

export function Paragraph({ text, className }) {
    return <p className={className}>{text}</p>
}