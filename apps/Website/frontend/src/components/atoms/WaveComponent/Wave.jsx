import './Wave.scss';

export function Wave({ top }) {
    return(
        <div className="wave" style={top ? {"top": `${top}px`} : {}}></div>
    )
}