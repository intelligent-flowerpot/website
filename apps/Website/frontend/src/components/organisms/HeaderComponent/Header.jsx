import Navbar from "../../molecules/NavbarComponent/Navbar"
import './Header.scss'

export function Header() {
    return(
        <header className="header">
            <Navbar />
        </header>
    )
}