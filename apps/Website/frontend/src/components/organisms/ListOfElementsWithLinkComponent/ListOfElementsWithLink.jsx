import { ListElementWithLink } from '../../molecules/ListElementWithLinkComponent/ListElementWithLink';
import { Paragraph } from '../../atoms/ParagraphComponent/Paragraph';
import './ListOfElementsWithLink.scss';

export function ListOfElementsWithLink({ elements }) {
    return(
        <div className="listOfElementsWithLink">

            <Paragraph
                text="Here you can find a list of all of your kits. The current humidity percentage is displayed"
                className="paragraph"
            />
            <ul className="listOfElementsWithLink__list">
                {elements.map((element) => (
                    <ListElementWithLink
                        key={element.id}
                        id={element.id}
                        name={`${element.name} at ${element.location}`}
                        value={element.value}
                    />
                ))}
            </ul>
        </div>
    )
}