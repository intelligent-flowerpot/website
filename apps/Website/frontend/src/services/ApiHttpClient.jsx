export const ApiHttpRequest = async (path, method = 'GET') => {
    const url = `https://api-riego.duckdns.org${path}`;

    try {
        const response = await fetch(
            url,
            {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
        );

        const responseJson = await response.json();
        return responseJson;
    } catch (error) {
        return {
            status: 'KO',
            error: error.message
        }
    }
}
