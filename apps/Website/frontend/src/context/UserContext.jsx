import React, { useState, useContext } from "react";

const UserContext = React.createContext();

export function UserContextProvider({children}) {
    const [user, setUser] = useState()

    return<UserContext.Provider value={{user, setUser}}>{children}</UserContext.Provider>
}

export function useUser() {
    const context = React.useContext(UserContext);
    if (context === undefined) {
      throw new Error('useCount must be used within a CountProvider');
    }
    return context;
}