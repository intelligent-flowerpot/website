import './App.scss'
import { Header } from './components/organisms/HeaderComponent/Header'
import { HomePage } from './pages/Home/HomePage';
import { LoginPage } from './pages/Login/LoginPage';
import { SignUpPage } from './pages/SignUp/SignUpPage';
import { ListOfKitsPage } from './pages/ListOfKits/ListOfKitsPage';
import { Route } from 'wouter';
import { UserContextProvider } from './context/UserContext';
import { KitPage } from './pages/Kit/KitPage';
import { LogoutPage } from './pages/Logout/LogoutPage';

// supertokens
import SuperTokens from "supertokens-auth-react";
import EmailPassword from "supertokens-auth-react/recipe/emailpassword";
import Session from "supertokens-auth-react/recipe/session";

SuperTokens.init({
    appInfo: {
        appName: "rieguitoV2",
        apiDomain: "https://api-riego.duckdns.org",
        websiteDomain: "https://app-riego.duckdns.org",
        apiBasePath: "/auth",
        websiteBasePath: "/login",
    },
    recipeList: [
        EmailPassword.init(),
        Session.init()
    ]
});

export default function App() {
  return (
    <div className='App'>
      <UserContextProvider>
        <Header />

        <Route
          component={HomePage}
          path="/"
        />
        <Route
          component={LoginPage}
          path="/login"
        />
        <Route
          component={LogoutPage}
          path="/logout"
        />
        <Route
          component={SignUpPage}
          path="/signup"
        />
        <Route
          component={ListOfKitsPage}
          path="/kits"
        />
        <Route
          component={KitPage}
          path="/kit/:id"
        />
      </UserContextProvider>
    </div>
  )
}
