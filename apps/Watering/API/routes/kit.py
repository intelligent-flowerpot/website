from fastapi import APIRouter
from ..config.db import conn
from ..models.kit import kits
from ..schemas.kit import Kit

kit = APIRouter()

# retrieve all kits 
@kit.get("/kits")
def get_kits():
    return conn.execute(kits.select()).fetchall()

# create a kit 
@kit.post("/kits")
def create_kit(kit: Kit):

    new_kit = {
        "kit_id": kit.id,
        "name": kit.name,
        "location_name": kit.location_name,
        "user_id": kit.user_id
        }
    return conn.execute(kits.insert().values(new_kit))

# retrieve kit by id
@kit.get("/kits/{id}")
def get_kit_by_id(id: str):
    return conn.execute(kits.select().where(kits.c.kit_id == id)).first()

# update kit by id 
@kit.put("/kits/{id}")
def update_kit(id: str, kit: Kit):
    return conn.execute(kits.update().values(name=kit.name, location_name=kit.location_name).where(kits.c.kit_id == id))

# delete a kit by id
@kit.delete("/kits/{id}")
def delete_kit(id: str):
    return conn.execute(kits.delete().where(kits.c.kit_id == id)).first()

#retrieve all user kits
def get_user_kits(user_id: int):
    return conn.execute(kits.select().where(kits.c.user_id == user_id)).fetchall()

# check if kit exists and api key is correct 
def check_valid_request(kit_id: str, api_key: str ):

    kit = get_kit_by_id(kit_id)

    if kit != None and kit['api_key'] == api_key:
        return True
    else:
        return False