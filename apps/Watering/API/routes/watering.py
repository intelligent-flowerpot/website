from fastapi import APIRouter, Body
from operator import and_
from datetime import datetime, timedelta
import json

from ..config.db import conn
from ..models.watering_history import watering_history
from ..schemas.watering_history import WateringHistory
from ..models.sensor_data_history import sensor_data_history
from ..schemas.sensor_data_history import SensorDataHistory
from .kit import check_valid_request 

watering = APIRouter()

N_WEEKS_AGO = 13

#
# WATERING LOGIC
#

IS_RAINING_VALUE = 0
SOIL_MOISTURE_DRY_VALUE = 4095
SOIL_MOISTURE_WATERING_VALUE = SOIL_MOISTURE_DRY_VALUE * 0.8

# decide wether to water or not
def is_watering_time(temperature: int, soil_moisture: int, rain: int):

    if rain == IS_RAINING_VALUE:
        return False
    
    if soil_moisture >= SOIL_MOISTURE_WATERING_VALUE:
        return True

    return False

#
# ARDUINO LOGIC
#

# recive arduino sensor data
@watering.post("/sensor_data")
def post_arduino_sensor_data(data: str = Body(None)):

    data = json.loads(data) 

    if not check_valid_request(data['kit_id'], data['api_key']):
        return "no such kit"

    watering_time = is_watering_time(data['temperature'], data['soil_moisture'], data['rain'])

    # check if arduino was already watering
    if data['watering']:

        # watering and water -> REGAR
        if watering_time:
            return "REGAR"

        # watering and no water -> NO REGAR
        update_watering_history(data['kit_id'])
        return "NO REGAR"

    else:

        # no watering and no water -> NO REGAR
        create_sensor_data_entry({
            "kit_id": data['kit_id'], 
            "temperature": data['temperature'], 
            "soil_moisture": data['soil_moisture'], 
            "rain": data['rain']
        })

        # no watering and water -> REGAR
        if watering_time:

            create_watering_history({
                "kit_id": data['kit_id'], 
                "temperature": data['temperature'], 
                "soil_moisture": data['soil_moisture'], 
                "rain": data['rain']
            })

            return "REGAR"

    return "NO REGAR"

#
# SENSOR DATA HISTORY
#
    
# retrieve all sensor data of given kit within N weeks ago
@watering.get("/sensor_data_history/{kit_id}")
def get_sensor_data_history(kit_id: str):

    return conn.execute(sensor_data_history.select().where(and_(
            sensor_data_history.c.kit_id == kit_id, 
            sensor_data_history.c.date_add > weeks_ago(N_WEEKS_AGO)))
        ).fetchall()

# retrieve all sensor data of given kit 
@watering.get("/all_sensor_data_history/{kit_id}")
def get_all_sensor_data_history(kit_id: str):

    return conn.execute(sensor_data_history.select().where(sensor_data_history.c.kit_id == kit_id)).fetchall()

# retrieve last sensor data value of given kit
@watering.get("/sensor_data_history/{kit_id}/last")
def get_last_sensor_data_history(kit_id: str):

    return conn.execute(sensor_data_history.select().where(
        sensor_data_history.c.kit_id == kit_id).order_by(sensor_data_history.c.id.desc())).first()

# create sensor data entry
def create_sensor_data_entry(sdh: SensorDataHistory):
    new_sdh = {
        "kit_id": sdh['kit_id'],
        "date_add": datetime.now(),
        "temperature": sdh['temperature'],
        "soil_moisture": sdh['soil_moisture'], 
        "rain": sdh['rain']
        }
    return conn.execute(sensor_data_history.insert().values(new_sdh))

#
# WATERING HISTORY
#    

# retrieve all watering history of given kit within N weeks ago
@watering.get("/watering_history/{kit_id}")
def get_watering_history(kit_id: str):

    return conn.execute(watering_history.select().where(and_(
            watering_history.c.kit_id == kit_id, 
            watering_history.c.date_start > weeks_ago(N_WEEKS_AGO)))
        ).fetchall()

# retrieve all watering history data of given kit 
@watering.get("/all_watering_history/{kit_id}")
def get_all_watering_history(kit_id: str):

    return conn.execute(watering_history.select().where(watering_history.c.kit_id == kit_id)).fetchall()

# create a entry
def create_watering_history(wh: WateringHistory):
    new_wh = {
        "kit_id": wh['kit_id'],
        "date_start": datetime.now(),
        "temperature": wh['temperature'],
        "soil_moisture": wh['soil_moisture'], 
        "rain": wh['rain']
        }
    return conn.execute(watering_history.insert().values(new_wh))

# update watering history entry
def update_watering_history(kit_id: str):
    return conn.execute(watering_history.update().values( date_end=datetime.now())
        .where(watering_history.c.kit_id == kit_id))

# return date of N weeks ago
def weeks_ago(weeks: int):
    return datetime.now() - timedelta(weeks=weeks)
