from fastapi import APIRouter
from ..config.db import conn
from ..models.user import users
from ..schemas.user import User
from cryptography.fernet import Fernet

from .kit import get_user_kits

user = APIRouter()

# encrypt pwd
f = Fernet(Fernet.generate_key())

# retrieve all users 
@user.get("/users")
def get_users():
    return conn.execute(users.select()).fetchall()

# create a user 
@user.post("/users")
def create_user(user: User):
    new_user = {
        "name": user.name,
        "email": user.email,
        "password": f.encrypt(user.password.encode("utf-8"))
        }
    return conn.execute(users.insert().values(new_user))

# retrieve a user by id
@user.get("/users/{id}")
def get_user_by_id(id: str):
    return conn.execute(users.select().where(users.c.id == id)).first()

# update user by id 
@user.put("/users/{id}")
def update_user(id: str, user: User):
    return conn.execute(users.update().values(name=user.name, email=user.email).where(users.c.id == id))

# delete a user 
@user.delete("/users/{id}")
def delete_user(id: str):
    return conn.execute(users.delete().where(users.c.id == id)).first

# retrieve all user kits
@user.get("/users/{id}/kits")
def get_user_by_id(id: str):
    return get_user_kits(id)
