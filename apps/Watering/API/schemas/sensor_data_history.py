from datetime import datetime
from pydantic import BaseModel
from typing import Optional

class SensorDataHistory(BaseModel):
    id: Optional[int]
    kit_id: str
    date_add: Optional[datetime]
    temperature: int
    soil_moisture: int
    rain: int