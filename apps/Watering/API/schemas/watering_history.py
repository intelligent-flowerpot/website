from datetime import datetime
from pydantic import BaseModel
from typing import Optional

class WateringHistory(BaseModel):
    id: Optional[int]
    kit_id: str
    date_start: Optional[datetime]
    date_end: Optional[datetime]
    temperature: int
    soil_moisture: int
    rain: int