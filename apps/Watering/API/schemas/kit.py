from pydantic import BaseModel
from typing import Optional

class Kit(BaseModel):
    id: str
    name: str
    location_name: str
    api_key: Optional[str]
    user_id: int