from pydantic import BaseModel
from typing import Optional

class User(BaseModel):
    id: Optional[int]
    name: str
    email: str
    password: str

    def asdict(self):
        return {'id': self.id, 'name': self.name, 'email': self.email, 'password': self.password}