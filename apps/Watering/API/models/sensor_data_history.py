from sqlalchemy import DateTime, ForeignKey, Table, Column
from sqlalchemy.sql.sqltypes import Integer
from ..config.db import meta, engine

sensor_data_history = Table("sensor_data_history", meta,
             Column("id", Integer, primary_key=True),
             Column("kit_id", ForeignKey("kits.kit_id")),
             Column("date_add", DateTime(timezone=True)),
             Column("temperature", Integer),
             Column("soil_moisture", Integer),
             Column("rain", Integer))

meta.create_all(engine)