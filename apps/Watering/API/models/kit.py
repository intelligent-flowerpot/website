from sqlalchemy import ForeignKey, Table, Column
from sqlalchemy.sql.sqltypes import String
from ..config.db import meta, engine

kits = Table("kits", meta,
             Column("kit_id", String(255), primary_key=True),
             Column("name", String(255)),
             Column("location_name", String(255)),
             Column("api_key", String(255), default="b6b1eb5be785e7858cbb196993fad39a"),
             Column("user_id", ForeignKey("users.user_id")))

meta.create_all(engine)