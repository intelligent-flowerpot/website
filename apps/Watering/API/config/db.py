from sqlalchemy import create_engine, MetaData
from dotenv import dotenv_values

config = dotenv_values()

engine = create_engine(f"mysql+pymysql://{config['DB_USER']}:{config['DB_PASSWORD']}@{config['DB_HOST']}:{config['DB_PORT']}/{config['DB_NAME']}")

meta = MetaData()

conn = engine.connect()
