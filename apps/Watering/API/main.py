from fastapi import FastAPI

from dotenv import dotenv_values
config = dotenv_values()

# routes 
from .routes.test import test
from .routes.user import user
from .routes.kit import kit
from .routes.watering import watering

# supertokens 
from supertokens_python import init, InputAppInfo, SupertokensConfig
from supertokens_python.recipe import emailpassword, session

# supertokens Middlewares
from supertokens_python import get_all_cors_headers
from starlette.middleware.cors import CORSMiddleware
from supertokens_python.framework.fastapi import Middleware

init(
    app_info=InputAppInfo(
        app_name="rieguitoV2",
        api_domain=config['API_DOMAIN'],
        website_domain=config['WEBSITE_DOMAIN'],
        api_base_path="/auth",
        website_base_path="/login"
    ),
    supertokens_config=SupertokensConfig(
        connection_uri=config['SUPERTOKENS_URI'],
        # api_key="IF YOU HAVE AN API KEY FOR THE CORE, ADD IT HERE"
    ),
    framework='fastapi',
    recipe_list=[
        session.init(), # initializes session features
        emailpassword.init()
    ],
    mode='asgi' # use wsgi if you are running using gunicorn
)

app = FastAPI()
app.add_middleware(Middleware)

# include routes here 
app.include_router(test)
app.include_router(user)
app.include_router(kit)
app.include_router(watering)

# add Middleware to all routes 
app = CORSMiddleware(
    app=app,
    allow_origins=[
        "*"
    ],
    allow_credentials=True,
    allow_methods=["GET", "PUT", "POST", "DELETE", "OPTIONS", "PATCH"],
    allow_headers=["Content-Type"] + get_all_cors_headers(),
)
